--profile
you have to use local profile
in intelliJ for example:
inVM options set: -Dspring.profiles.active=local

--rest:
You can trigger http requests here:
swagger
http://localhost:8080/swagger-ui.html

--DB:
when application is running go to:

http://localhost:8080/h2-console/

set jdbc url
jdbc:h2:mem:testdb (taken from spring.datasource.url property)
and log in with credentials:
sa/empty password

-- how to log in:
first open swagger (look --rest paragraph)
create user
then go to http://localhost:8080/user/profile/ and login with email and password
(default form is used to login)

