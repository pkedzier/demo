package com.example.demo.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/swagger-ui.html").permitAll()
                .antMatchers("/h2_console/").permitAll()
                .antMatchers("/h2_console/**").permitAll()
                .antMatchers("/user/create/").permitAll()
                .antMatchers("/user/**").authenticated();



        http.csrf().disable();
        http.headers().frameOptions().disable();
        http.formLogin();
    }
}
