package com.example.demo.user.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue
    private long id;

    private String firstName;

    private String lastName;

    private String password;

    private String email;

    @ManyToMany
    @JoinTable(name = "user_friends",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "friend_id"))
    protected List<User> friends = new ArrayList();
    @ManyToMany(mappedBy = "friends")
    protected List<User> befriended = new ArrayList();


    public void addFriend(User newFriend){
        synchronized (this){
            friends.add(newFriend);
            newFriend.getBefriended().add(this);
        }
    }

    public void removeFriend(User friendToRemove){
        synchronized(this){
            friends.removeIf(f -> f.getId() == friendToRemove.getId());
            friendToRemove.getBefriended().removeIf(f -> f.getId() == this.id);
        }
    }

    public boolean isContainedInFriendList(User friend) {
        return getFriends().stream().anyMatch(u -> u.getId() == friend.getId());
    }
}
