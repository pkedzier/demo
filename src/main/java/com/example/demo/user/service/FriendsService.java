package com.example.demo.user.service;

import com.example.demo.user.model.User;
import com.example.demo.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

import static java.lang.String.format;

@Service
public class FriendsService {

    @Autowired
    private UserSearchService userSearchService;

    @Autowired
    private UserRepository userRepository;

    public void addFriend(String email, long newFriendID){
        Optional<User> user = userSearchService.findUserByEmail(email);
        Optional<User> newFriend = userSearchService.findUserById(newFriendID);


        if(areUsersPresent(user, newFriend)){

            validateFriendshipRequest(user.get(), newFriend.get());

            addFriend(user.get(), newFriend.get());
        }
        else{
            throw new IllegalStateException("Operation not allowed.");
        }
    }

    private boolean areUsersPresent(Optional<User> ... users) {
        return Arrays.stream(users)
                .allMatch(Optional::isPresent);
    }

    private void validateFriendshipRequest(User user, User newFriend) {
        if(areAlreadyFriends(user, newFriend)){
            throw new IllegalStateException(
                    format("Given users %s and %s are already friends.",user.getEmail(), newFriend.getEmail()));
        }
    }

    private boolean areAlreadyFriends(User user, User newFriend) {
        return Stream.of(user.getBefriended(), user.getFriends())
                .flatMap(x -> x.stream())
                .anyMatch(u -> u.getId() == newFriend.getId());
    }

    private void addFriend(User user, User newFriend) {
        user.addFriend(newFriend);
        userRepository.save(user);
    }

    public void removeFriend(String email, long friendToRemoveID){
        Optional<User> user = userSearchService.findUserByEmail(email);
        Optional<User> friendToRemove = userSearchService.findUserById(friendToRemoveID);

        if(areUsersPresent(user, friendToRemove)){

            validateFriendshipRemovalRequest(user.get(), friendToRemove.get());

            removeFriend(user.get(), friendToRemove.get());
        }
        else{
            throw new IllegalStateException("Operation not allowed.");
        }
    }

    private void validateFriendshipRemovalRequest(User user, User friend) {
        if(!areAlreadyFriends(user, friend)){
            throw new IllegalStateException(
                    format("You can't remove friendship between %s and %s", user.getEmail(), friend.getEmail()));
        }
    }

    private void removeFriend(User user, User friend) {
        if(user.isContainedInFriendList(friend)){
            user.removeFriend(friend);
            userRepository.save(user);
        }
        else if(friend.isContainedInFriendList(user)){
            friend.removeFriend(user);
            userRepository.save(friend);
        }


    }
}
