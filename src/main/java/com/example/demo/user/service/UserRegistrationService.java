package com.example.demo.user.service;

import com.example.demo.user.model.User;
import com.example.demo.user.repository.UserRepository;
import com.example.demo.user.web.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service
public class UserRegistrationService {

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserSearchService userSearchService;

    public void createUserAccount(UserDTO userDto){

        if(isCreatedAccount(userDto)){
            throw new IllegalStateException(
                    format("Account for given email: %s has been already created.", userDto.getEmail()));
        }

        User user = User.builder()
                            .firstName(userDto.getFirstName())
                            .lastName(userDto.getLastName())
                            .email(userDto.getEmail())
                            .password(encoder.encode(userDto.getPassword()))
                            .build();

        userRepository.save(user);
    }

    public boolean isCreatedAccount(UserDTO userDTO){
        return userSearchService.findUserByEmail(userDTO.getEmail()).isPresent();
    }
}
