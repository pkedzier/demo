package com.example.demo.user.service;

import com.example.demo.user.model.User;
import com.example.demo.user.repository.UserRepository;
import com.example.demo.user.web.dto.UserCardDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserSearchService {

    @Autowired
    private UserRepository userRepository;

    public UserCardDTO findUserCardByEmail(String email){
        Optional<User> user = findUserByEmail(email);
        return user.map(this::mapToUserCard).orElse(null);
    }

    private UserCardDTO mapToUserCard(User user) {
        return UserCardDTO.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .id(user.getId())
                .build();
    }

    public Optional<User> findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public Optional<User> findUserById(long id){
        return userRepository.findById(id);
    }

}
