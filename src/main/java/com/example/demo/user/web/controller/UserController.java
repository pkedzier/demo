package com.example.demo.user.web.controller;

import com.example.demo.user.service.FriendsService;
import com.example.demo.user.service.UserRegistrationService;
import com.example.demo.user.service.UserSearchService;
import com.example.demo.user.web.dto.UserCardDTO;
import com.example.demo.user.web.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Slf4j
@RestController
@RequestMapping(value="/user")
public class UserController {

    @Autowired
    private UserRegistrationService userRegistrationService;

    @Autowired
    private UserSearchService userSearchService;

    @Autowired
    private FriendsService friendsService;

    @PostMapping(value = "/create/")
    public void createUser(@RequestBody UserDTO userDto){
        log.debug("Register user {}", userDto.getEmail());

        userRegistrationService.createUserAccount(userDto);
    }

    @GetMapping(value = "/profile/")
    public String showProfile(){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return "You are logged in to account: " + userDetails.getUsername();
    }

    @GetMapping(value = "/profile/search/")
    public UserCardDTO findUserProfile(@RequestParam(value = "email") String email){
        return userSearchService.findUserCardByEmail(email);
    }

    @PostMapping(value = "/profile/{profileId}/friend/request/")
    public void createFriend(@PathVariable(value = "profileId") long profileId){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        log.debug("Friendship request: " + profileId + " sent by: " + userDetails.getUsername());

        friendsService.addFriend(userDetails.getUsername(), profileId);
    }
    @PostMapping(value = "/profile/{profileId}/friend/removal/")
    public void removeFriend(@PathVariable(value = "profileId") long profileId){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        log.debug("Friendship remvoal request: " + profileId + " sent by: " + userDetails.getUsername());

        friendsService.removeFriend(userDetails.getUsername(), profileId);
    }


}
