package com.example.demo.user.web.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserDTO {
    private String firstName;

    private String lastName;

    private String password;

    private String email;
}
