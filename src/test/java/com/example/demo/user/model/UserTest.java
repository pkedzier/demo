package com.example.demo.user.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {

    @Test
    public void shouldReturnTrueWhenIsFriend(){
        //given
        User user = new User();
        User friend = new User();
        user.addFriend(friend);

        //when
        boolean isFriend = user.isContainedInFriendList(friend);

        //then
        assertTrue(isFriend);
    }

    @Test
    public void shouldReturnFalseWhenIsNotFriend(){
        //given
        User user = new User();
        User friend = new User();

        //when
        boolean isFriend = user.isContainedInFriendList(friend);

        //then
        assertFalse(isFriend);
    }

    @Test
    public void shouldAddFriend(){
        //given
        User user = new User();
        User friend = new User();

        //when
        user.addFriend(friend);

        //then
        assertFalse(user.getFriends().isEmpty());
        assertFalse(friend.getBefriended().isEmpty());
        assertEquals(friend, user.getFriends().get(0));
        assertEquals(user, friend.getBefriended().get(0));
    }
    @Test
    public void shouldRemoveFriend(){
        //given
        User user = new User();
        User friend = new User();

        user.getFriends().add(friend);
        friend.getBefriended().add(user);

        //when
        user.removeFriend(friend);

        //then
        assertTrue(user.getFriends().isEmpty());
        assertTrue(friend.getBefriended().isEmpty());
    }
}