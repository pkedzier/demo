package com.example.demo.user.service;

import com.example.demo.user.model.User;
import com.example.demo.user.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Optional;

import static java.util.Optional.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class FriendsServiceTest {

    public static final long NEW_FRIEND_ID = 101L;
    public static final String USER_EMAIL = "test@test.com";

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserSearchService userSearchService;

    @InjectMocks
    private FriendsService friendsService;


    @Test(expected = IllegalStateException.class)
    public void shoulThrowExceptionWhenAddFriendAndUserAndFriendNotSavedInDB(){
       //given
        when(userSearchService.findUserByEmail(USER_EMAIL)).thenReturn(empty());
        when(userSearchService.findUserById(NEW_FRIEND_ID)).thenReturn(empty());

       //when
        friendsService.addFriend(USER_EMAIL, NEW_FRIEND_ID);
       //then
    }

    @Test(expected = IllegalStateException.class)
    public void shoulThrowExceptionWhenAddFriendAndUserNotSavedInDB(){
        //given
        when(userSearchService.findUserByEmail(USER_EMAIL)).thenReturn(empty());
        when(userSearchService.findUserById(NEW_FRIEND_ID)).thenReturn(createFriend());

        //when
        friendsService.addFriend(USER_EMAIL, NEW_FRIEND_ID);
        //then
    }

    @Test(expected = IllegalStateException.class)
    public void shoulThrowExceptionWhenAddFriendAndFriendNotSavedInDB(){
        //given
        when(userSearchService.findUserByEmail(USER_EMAIL)).thenReturn(createUser());
        when(userSearchService.findUserById(NEW_FRIEND_ID)).thenReturn(empty());

        //when
        friendsService.addFriend(USER_EMAIL, NEW_FRIEND_ID);
        //then
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotRequestsTheSameFriendshipAgain(){
        //given
        Optional<User> user = createUser();
        Optional<User> friend = createFriend();
        user.get().addFriend(friend.get());
        when(userSearchService.findUserByEmail(USER_EMAIL)).thenReturn(user);
        when(userSearchService.findUserById(NEW_FRIEND_ID)).thenReturn(friend);

        //when
        friendsService.addFriend(USER_EMAIL, NEW_FRIEND_ID);

        //then
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionIfFriendAddedUserBeforeCurrentRequest(){
        //given
        Optional<User> user = createUser();
        Optional<User> friend = createFriend();
        friend.get().addFriend(user.get());
        when(userSearchService.findUserByEmail(USER_EMAIL)).thenReturn(user);
        when(userSearchService.findUserById(NEW_FRIEND_ID)).thenReturn(friend);

        //when
        friendsService.addFriend(USER_EMAIL, NEW_FRIEND_ID);

        //then
    }

    @Test
    public void shouldAddFriend(){
        //given
        Optional<User> user = createUser();
        Optional<User> friend = createFriend();
        when(userSearchService.findUserByEmail(USER_EMAIL)).thenReturn(user);
        when(userSearchService.findUserById(NEW_FRIEND_ID)).thenReturn(friend);

        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);

        //when
        friendsService.addFriend(USER_EMAIL, NEW_FRIEND_ID);

        //then
        verify(userRepository).save(userCaptor.capture());
        User savedUser = userCaptor.getValue();

        assertEquals(1, savedUser.getFriends().size());

        User savedFriend = savedUser.getFriends().get(0);
        assertEquals(friend.get(), savedFriend);
        assertEquals(1, savedFriend.getBefriended().size());
        assertEquals(savedFriend.getBefriended().get(0), savedUser);
    }

    @Test(expected = IllegalStateException.class)
    public void shoulNotRemoveFriendIfUserAndFriendAreNotSavedInDB(){
        //given
        when(userSearchService.findUserByEmail(USER_EMAIL)).thenReturn(empty());
        when(userSearchService.findUserById(NEW_FRIEND_ID)).thenReturn(empty());

        //when
        friendsService.removeFriend(USER_EMAIL, NEW_FRIEND_ID);
        //then
    }

    @Test(expected = IllegalStateException.class)
    public void shoulNotRemoveFriendIfUserIsNotSavedInDB(){
        //given
        when(userSearchService.findUserByEmail(USER_EMAIL)).thenReturn(empty());
        when(userSearchService.findUserById(NEW_FRIEND_ID)).thenReturn(createFriend());

        //when
        friendsService.removeFriend(USER_EMAIL, NEW_FRIEND_ID);
        //then
    }

    @Test(expected = IllegalStateException.class)
    public void shoulNotRemoveFriendIfFriendIsNotSavedInDB(){
        //given
        when(userSearchService.findUserByEmail(USER_EMAIL)).thenReturn(createUser());
        when(userSearchService.findUserById(NEW_FRIEND_ID)).thenReturn(empty());

        //when
        friendsService.removeFriend(USER_EMAIL, NEW_FRIEND_ID);
        //then
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenRemoveUserWhoIsNotHisFriend(){
        //given
        when(userSearchService.findUserByEmail(USER_EMAIL)).thenReturn(createUser());
        when(userSearchService.findUserById(NEW_FRIEND_ID)).thenReturn(createFriend());

        //when
        friendsService.removeFriend(USER_EMAIL, NEW_FRIEND_ID);
        //then
    }

    @Test
    public void shouldRemoveUserIfUserRequestedFriendship(){
        //given
        Optional<User> user = createUser();
        Optional<User> friend = createFriend();
        user.get().addFriend(friend.get());

        when(userSearchService.findUserByEmail(USER_EMAIL)).thenReturn(user);
        when(userSearchService.findUserById(NEW_FRIEND_ID)).thenReturn(friend);
        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        //when
        friendsService.removeFriend(USER_EMAIL, NEW_FRIEND_ID);

        //then
        verify(userRepository).save(userCaptor.capture());
        User savedUser = userCaptor.getValue();
        assertTrue(savedUser.getFriends().isEmpty());
    }

    @Test
    public void shouldRemoveUserIfFriendRequestedFriendship(){
        //given
        Optional<User> user = createUser();
        Optional<User> friend = createFriend();
        friend.get().addFriend(user.get());

        when(userSearchService.findUserByEmail(USER_EMAIL)).thenReturn(user);
        when(userSearchService.findUserById(NEW_FRIEND_ID)).thenReturn(friend);
        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        //when
        friendsService.removeFriend(USER_EMAIL, NEW_FRIEND_ID);

        //then
        verify(userRepository).save(userCaptor.capture());
        User savedUser = userCaptor.getValue();
        assertTrue(savedUser.getBefriended().isEmpty());
    }

    private Optional<User> createUser() {
        User friend = User.builder()
                .email(USER_EMAIL)
                .befriended(new ArrayList<>())
                .friends(new ArrayList<>())
                .build();

        return Optional.of(friend);
    }

    private Optional<User> createFriend() {
        User friend = User.builder()
                .id(NEW_FRIEND_ID)
                .befriended(new ArrayList<>())
                .friends(new ArrayList<>())
                .build();

        return Optional.of(friend);
    }
}