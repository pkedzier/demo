package com.example.demo.user.service;

import com.example.demo.user.model.User;
import com.example.demo.user.repository.UserRepository;
import com.example.demo.user.web.dto.UserDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserRegistrationServiceTest {

    public static final String EMAIL = "test@test.com";
    public static final String FIRST_NAME = "John";
    public static final String LAST_NAME = "King";
    public static final String ENCODED_PASSWORD = "D9B9BEC3F4CC5482E7C5EF43143E563A";
    public static final String PASSWORD = "random text";
    @Mock
    private PasswordEncoder encoder;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserSearchService userSearchService;

    @InjectMocks
    private UserRegistrationService sut;

    @Test
    public void shouldReturnTrueIfUSerAccountCreated(){
        //given
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail(EMAIL);
        User user = Mockito.mock(User.class);

        //when
        when(userSearchService.findUserByEmail(EMAIL)).thenReturn(Optional.of(user));
        boolean isCreatedAccount = sut.isCreatedAccount(userDTO);

        //then
        assertTrue(isCreatedAccount);
    }

    @Test
    public void shouldReturnFalseIfUSerAccountDoesntExist(){
        //given
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail(EMAIL);

        //when
        when(userSearchService.findUserByEmail(EMAIL)).thenReturn(Optional.empty());
        boolean isCreatedAccount = sut.isCreatedAccount(userDTO);

        //then
        assertFalse(isCreatedAccount);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotCreateSecondOneAccountForSameMail(){
        //given
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail(EMAIL);
        User user = Mockito.mock(User.class);
        when(userSearchService.findUserByEmail(EMAIL)).thenReturn(Optional.of(user));

        //when
        sut.createUserAccount(userDTO);
    }

    @Test
    public void shouldCreateUser(){
        //given
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail(EMAIL);
        userDTO.setFirstName(FIRST_NAME);
        userDTO.setLastName(LAST_NAME);
        userDTO.setPassword(PASSWORD);

        when(userSearchService.findUserByEmail(EMAIL)).thenReturn(Optional.empty());
        when(encoder.encode(PASSWORD)).thenReturn(ENCODED_PASSWORD);
        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);

        //when
        sut.createUserAccount(userDTO);

        //then
        verify(userRepository).save(userCaptor.capture());
        User savedUser = userCaptor.getValue();

        assertEquals(FIRST_NAME, savedUser.getFirstName());
        assertEquals(LAST_NAME, savedUser.getLastName());
        assertEquals(EMAIL, savedUser.getEmail());
        assertEquals(ENCODED_PASSWORD, savedUser.getPassword());
    }
}