package com.example.demo.user.service;

import com.example.demo.user.model.User;
import com.example.demo.user.repository.UserRepository;
import com.example.demo.user.web.dto.UserCardDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserSearchServiceTest {

    public static final String EMAIL = "test@test.com";
    public static final String PASSWORD = "3A4A215074AC0650EEAB4A9F0B14A86E";
    public static final String LAST_NAME = "Tyson";
    public static final String FIRST_NAME = "Robert";
    public static final long ID = 151;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserSearchService userSearchService;

    @Test
    public void shouldFindUserCardByEmail(){
        //given
        when(userRepository.findByEmail(EMAIL)).thenReturn(createUser());

        //when
        UserCardDTO userCard = userSearchService.findUserCardByEmail(EMAIL);

        //then
        assertEquals(FIRST_NAME, userCard.getFirstName());
        assertEquals(LAST_NAME, userCard.getLastName());
        assertEquals(ID, userCard.getId());
    }

    @Test
    public void shouldFindUserCardByEmailAndMapNullValues(){
        //given
        when(userRepository.findByEmail(EMAIL)).thenReturn(createEmptyUser());

        //when
        UserCardDTO userCard = userSearchService.findUserCardByEmail(EMAIL);

        //then
        assertEquals(null, userCard.getFirstName());
        assertEquals(null, userCard.getLastName());
        assertEquals(0, userCard.getId());
    }

    @Test
    public void shouldFindUserByEmail(){
        //given
        Optional<User> user = createUser();
        when(userRepository.findByEmail(EMAIL)).thenReturn(user);

        //when
        Optional<User> userResult = userSearchService.findUserByEmail(EMAIL);

        //then
        assertEquals(user, userResult);
    }

    @Test
    public void shouldFindUserByID(){
        //given
        Optional<User> user = createUser();
        when(userRepository.findById(ID)).thenReturn(user);

        //when
        Optional<User> userResult = userSearchService.findUserById(ID);

        //then
        assertEquals(user, userResult);
    }

    private Optional<User> createUser() {
        User user = User.builder()
                .email(EMAIL)
                .password(PASSWORD)
                .lastName(LAST_NAME)
                .firstName(FIRST_NAME)
                .befriended(new ArrayList<>())
                .friends(new ArrayList<>())
                .id(ID)
                .build();

        return Optional.of(user);
    }

    private Optional<User> createEmptyUser(){
        return Optional.of(new User());
    }
}